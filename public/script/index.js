var socket = io();

socket.on('connect', function() {
    console.log("Connected to the server");
});

socket.on('disconnect', function() {
    console.log("Disconnected from server");
});

// socket.emit('createMessage', {
//     to: "Sumeet Dewangan",
//     text: "Hello World this is Sumeet"
// });

socket.on('newMessage', function(message) {
    var div = document.getElementById('message-div');
    div.innerHTML += div.innerHTML + JSON.stringify(message);
});