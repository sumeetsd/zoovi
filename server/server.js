const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const PORT = process.env.PORT || 3000;
const public = path.join(__dirname, '../public');

var app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.use(express.static(public));

io.on('connection', (socket) => {
    console.log("New user connected");
    
    // socket.emit('newMessage', {
    //     from: "Sumeet Dewangan",
    //     text: "Hello World",
    //     time: new Date().toString()
    // });

    socket.on('createMessage', (message) => {
        console.log("\nNew Message recieved")
        
        io.emit('newMessage', {
            to: message.to,
            text: message.text,
            createdAt: new Date().getTime()
        });
    });

    socket.on('disconnect', () => {
        console.log("User Disconnected");
    });
});

server.listen(PORT, () => {
    console.log("Started listining on port 3000");
});